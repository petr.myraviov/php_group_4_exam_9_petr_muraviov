<?php

namespace App\Orchid\Layouts;

use App\Comment;
use Orchid\Screen\Actions\Link;
use Orchid\Screen\Layouts\Table;
use Orchid\Screen\TD;

class CommentListLayout extends Table
{
    /**
     * Data source.
     *
     * The name of the key to fetch it from the query.
     * The results of which will be elements of the table.
     *
     * @var string
     */
    protected $target = 'comments';

    /**
     * Get the table cells to be displayed.
     *
     * @return TD[]
     */
    protected function columns(): array
    {
        return [
            TD::set('title', 'Title')
                ->sort()
                ->filter(TD::FILTER_TEXT)
                ->render(function (Comment $comment){
                    return Link::make($comment->body)
                        ->route('platform.comments.edit', $comment);
                }),
            TD::set('estimation', 'Estimation')
                ->sort()
                ->filter(TD::FILTER_TEXT)
                ->render(function (Comment $comment){
                    return Link::make($comment->estimation)
                        ->route('platform.comments.edit', $comment);
                }),
            TD::set('created_at', 'Created')->sort(),
            TD::set('updated_at', 'Last edit')->sort(),
        ];
    }
}
