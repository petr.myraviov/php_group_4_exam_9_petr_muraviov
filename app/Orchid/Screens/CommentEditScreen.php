<?php

namespace App\Orchid\Screens;

use App\Comment;
use App\Photo;
use App\User;
use App\Http\Requests\CommentRequest;
use Facade\Ignition\QueryRecorder\Query;
use Illuminate\Http\Request;
use Orchid\Screen\Action;
use Orchid\Screen\Actions\Button;
use Orchid\Screen\Fields\Input;
use Orchid\Screen\Fields\Quill;
use Orchid\Screen\Fields\Relation;
use Orchid\Screen\Layout;
use Orchid\Screen\Screen;
use Orchid\Support\Facades\Alert;

class CommentEditScreen extends Screen
{
    /**
     * Display header name.
     *
     * @var string
     */
    public $name = 'Create comment';

    /**
     * Display header description.
     *
     * @var string
     */
    public $description = 'Mango comment';
    public $exists = false;

    /**
     * Query data.
     *
     * @param Comment $comment
     * @return array
     */
    public function query(Comment $comment): array
    {
            $this->exists = $comment->exists;
        if ($this->exists) {
            $this->name = "edit photo";
        }
        $comment->load('attachment');
        return [
            'comment' => $comment
        ];
    }

    /**
     * Button commands.
     *
     * @return Action[]
     */
    public function commandBar(): array
    {
        return [
            Button::make('Create comment')
                ->icon('icon-pencil')
                ->method('createOrUpdate')
                ->canSee(!$this->exists),

            Button::make('Update comment')
                ->icon('icon-note')
                ->method('createOrUpdate')
                ->canSee($this->exists),
            Button::make('Remove')
                ->icon('icon-trash')
                ->method('remove')
                ->canSee($this->exists),
        ];
    }

    /**
     * Views.
     *
     * @return Layout[]
     */
    public function layout(): array
    {
        return [
            Layout::rows([
                Quill::make('comment.body')
                    ->title('Body Comment')
                    ->required()
                    ->placeholder('Enter comment body'),
                Input::make('comment.estimation')
                    ->title('Estimation')
                    ->placeholder('Enter comment Estimation')
                    ->required(),
                Relation::make('comment.user_id')
                    ->title('User')
                    ->required()
                    ->fromModel(User::class, 'name', 'id'),
                Relation::make('comment.photo_id')
                    ->title('Photo')
                    ->required()
                    ->fromModel(Photo::class, 'image', 'id'),

            ])
        ];
    }

    /**
     * @param Comment $comment
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function createOrUpdate(Comment $comment, Request $request)
    {
        $comment->fill($request->get('comment'))->save();
        Alert::info('You has successfully created post');
        return redirect()->route('platform.comments.list');
    }

    /**
     * @param Comment $comment
     * @return \Illuminate\Http\RedirectResponse
     * @throws \Exception
     */
    public function remove(Comment $comment)
    {
        $comment->delete()
            ? Alert::info('Delete')
            : Alert::warning('No delete!');
        return redirect()->route('platform.comments.list');

    }
}
