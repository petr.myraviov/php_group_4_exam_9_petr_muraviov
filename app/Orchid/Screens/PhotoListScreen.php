<?php

namespace App\Orchid\Screens;
use Orchid\Screen\Layouts\Card;
use App\Photo;
use App\Orchid\Layouts\PhotoListLayout;
use Orchid\Screen\Action;
use Orchid\Screen\Actions\Link;
use Orchid\Screen\Layout;
use Orchid\Screen\Screen;

class PhotoListScreen extends Screen
{
    /**
     * Display header name.
     *
     * @var string
     */
    public $name = 'All photo';

    /**
     * Display header description.
     *
     * @var string
     */
    public $description = 'All photos description';

    /**
     * Query data.
     *
     * @return array
     */
    public function query(): array
    {
        return [
            'photos' => Photo::filters()->defaultSort('id')->paginate()
        ];
    }

    /**
     * Button commands.
     *
     * @return Action[]
     */
    public function commandBar(): array
    {
        return [
            Link::make('Create new photo')
                ->icon('icon-pencil')
                ->route('platform.photos.edit')
        ];
    }

    /**
     * Views.
     *
     * @return Layout[]
     */
    public function layout(): array
    {
        return [
            PhotoListLayout::class
        ];
    }
}
