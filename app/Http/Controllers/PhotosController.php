<?php

namespace App\Http\Controllers;

use App\Comment;
use App\Http\Requests\PhotoRequest;
use App\Photo;
use App\User;
use Illuminate\Http\Request;

class PhotosController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth')->except('index','profiles',);
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index()
    {
        return view('photos.index', ['photos' => Photo::all()]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('photos.create');
    }


    /**
     * @param PhotoRequest $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function store(PhotoRequest $request)
    {
        $photo = new Photo();
        $photo->title = $request->input('title');
        $photo->user_id = $request->user()->id;
        $file = $request->file('image');
        if (!is_null($file)){
            $path = $file->store('image', 'public');
            $photo['image'] = $path;
        }
        $photo->save();
        return redirect(route('photos.index'));
    }

    /**
     * @param Photo $photo
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function show(Photo $photo)
    {
        return view('photos.show',['photo' => $photo]);
    }


    /**
     * @param Photo $photo
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function edit(Photo $photo)
    {
        return view('photos.edit',['photo' => $photo]);
    }


    /**
     * @param PhotoRequest $request
     * @param Photo $photo
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function update(PhotoRequest $request, Photo $photo)
    {
        $photo->update($request->all());
        return redirect(route('photos.index'));
    }


    /**
     * @param Photo $photo
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     * @throws \Exception
     */
    public function destroy(Photo $photo)
    {
        $photo->delete();
        return redirect(route('photos.index'));
    }

    /**
     * @param User $user
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function profiles(User $user)
    {
        $comment = Comment::latest()->first();
        $photo = Photo::latest()->first();
        $collections= collect(['users' => User::all()]);
        return view('photos.profiles', [
            'collections' => $collections,
            'user' =>$user,
        ]);


    }


}
