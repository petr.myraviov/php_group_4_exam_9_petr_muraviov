<?php

namespace App\Http\Controllers;

use App\Comment;
use App\Http\Requests\CommentRequest;
use App\Photo;
use Illuminate\Http\Request;

class PhotoCommentsController extends Controller
{


    /**
     * @param CommentRequest $request
     * @param Photo $photo
     * @return \Illuminate\Http\JsonResponse
     * @throws \Throwable
     */
    public function store(CommentRequest $request, Photo $photo)
    {
        $comment = new Comment();
        $comment->body = $request->input('body');
        $comment->estimation =$request->input('estimation');
        $comment->photo_id = $photo->id;
        $comment->user_id = $request->user()->id;
        $comment->save();
        return response()->json([
            'comment' => view('components.comment', compact('comment'))->render()
        ], 201);
    }

}
