<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Route::group(['middleware'=>'language'],function () {
    Route::get('/home', 'HomeController@index')->name('home');
    Auth::routes();
    Route::get('/', "PhotosController@index");
    Route::get('photos.profiles/{user}', "PhotosController@profiles")->where('id','+id')->name('photos.profiles');
    Route::resource('photos','PhotosController');
    Route::resource('photos.comments', 'PhotoCommentsController')->only('store');

});

Route::get('language/{locale}', 'LanguageSwitcherController@switcher')
    ->name('language.switcher')
    ->where('locale', 'en|ru');
