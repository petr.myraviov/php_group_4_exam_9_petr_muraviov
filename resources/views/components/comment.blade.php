<div class="comment" id="delete-comment-{{$comment->id}}">
    @can('delete', $comment)
        <span data-comment-id="{{$comment->id}}" aria-hidden="true" class="delete">x</span>
        <div class="info">
            <form method="post" action="{{route('photos.comments.destroy', ['photo' => $comment->photo ,'comment' => $comment])}}">
                @method('delete')
                @csrf
                <button type="submit" class="active-product-area">Удалить</button>
            </form>
        </div>
    @endcan
    <div class="media g-mb-30 media-comment">
        <div class="media-body u-shadow-v18 g-bg-secondary g-pa-30">
            <div class="g-mb-15">
            </div>
            <h5 class="h5 g-color-gray-dark-v1 mb-0">{{ucfirst($comment->user->name)}}</h5>
            <p>
                {{ucfirst($comment->body)}}
            </p>
            <p>
                {{$comment->estimation}}
            </p>
            <span class="g-color-gray-dark-v4 g-font-size-12">{{$comment->created_at->diffForHumans()}}</span>
        </div>
    </div>
</div>
