@extends('layouts.app')
@section('content')
    <div class="container">
        <div class="row">
            @foreach($photos as $photo)

                <div class="modal fade" id="staticBackdrop-{{$photo->id}}" data-backdrop="static" data-keyboard="false" tabindex="-1" role="dialog" aria-labelledby="staticBackdropLabel" aria-hidden="true">
                    <div class="modal-dialog">
                        <div class="modal-content">
                            <div class="modal-header">

                                {{--                                @cannot(Auth::user())--}}

                                <a href="{{route('photos.profiles',['user' => $photo->user])}}">
                                    @csrf
                                    <h5 class="modal-title" id="staticBackdropLabel">{{$photo->user->name}}</h5>
{{--                                    @csrf--}}
                                </a>
{{--                                @endcannot--}}
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">

                                    <span aria-hidden="true">&times;</span>
                                </button>
                            </div>
                            <h4> {{$photo->title}}</h4>
                            <div class="modal-body">
                                <span class="likebtn-wrapper" data-theme="large" data-lang="ru" data-identifier="{{$photo->user->id}}"></span>
                                <div id="comments-block" class="col-8 scrollit comments-block">
                                    @foreach($photo->comments as $comment)
                                        <x-comment :comment="$comment"></x-comment>
                                    @endforeach
                                </div>
                                <div class="col-4">
                                    <div class="comment-form">
                                        <form id="create-comment-{{$photo->id}}" class="create-comment">
                                            @csrf
                                            @method('post')
                                            <input type="hidden" id="photo_id-{{$photo->id}}" value="{{$photo->id}}">
                                            <div class="form-group">
                                                <label for="body">Комментарий</label>
                                                <textarea name="body" class="form-control" id="body" rows="3" required></textarea>
                                            </div>
                                            <p><select id="estimation" name="estimation">
                                                    <option selected disabled>Выберите оценку</option>
                                                    <option value="1">1</option>
                                                    <option value="2">2</option>
                                                    <option value="3">3</option>
                                                    <option value="4">4</option>
                                                    <option value="5">5</option>
                                                </select></p>
                                            <button  id="create-comment-btn" data-photo-id="{{$photo->id}}" type="submit" class="btn btn-outline-primary btn-sm btn-block create-comment-btn">Добавьте новый
                                                комментарий
                                            </button>
                                        </form>
                                    </div>
                                </div>
                            </div>
                            <div class="modal-footer">
                                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                            </div>
                        </div>
                    </div>
                </div>
                @csrf
                <div style="margin: auto;padding-top: 20px" class="col-xl-4 col-md-4 col-lg-4">
                    <div  style="border-radius: 300px; height: 300px; width: 300px"  class="thumb">
                        <button type="button" style="border-radius: 300px; height: 300px; width: 300px;" data-toggle="modal" data-target="#staticBackdrop-{{$photo->id}}">
                            <img src="{{asset('storage/'.$photo->image)}}" class="card-img" style="border-radius: 300px; height: 300px; width: 300px; margin-left: -6px; margin-top: -2px"  alt="{{$photo->id}}">
                        </button>
                    </div>
                </div>

            @endforeach
        </div>
    </div>
@endsection
