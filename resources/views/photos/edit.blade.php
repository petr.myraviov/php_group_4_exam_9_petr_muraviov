@extends('layouts.app')
@section('content')
    <h1>Edit Photo</h1>
    <form  enctype="multipart/form-data" method="post" action="{{route('photos.update', $photo)}}">
        @method('put')
        @csrf
        <div class="custom-file">
            <label class="custom-label" for="validatedInputGroupCustomFile">Title</label>
            <input name="title" value="{{$photo->title}}" type="text">
        </div>
        <div class="input-group is-invalid">

            <div class="custom-file">
                <input  name="image" type="file" class="custom-file-input" id="validatedInputGroupCustomFile" value="{{$photo->image}}" required>
                <label class="custom-file-label" for="validatedInputGroupCustomFile">Upload photo...</label>
            </div>
            <div class="input-group-append">
                <button class="btn btn-outline-secondary" type="submit">Download</button>
            </div>
        </div>
    </form>
@endsection
