@extends('layouts.app')
@section('content')
    <h2>{{($photo->title)}}</h2>
        <img src="{{asset('storage/'.$photo->image)}}" class="card-img" style=" margin-left: -6px; margin-top: -2px"  alt="{{$photo->id}}">
    <div class="modal-body">
        <span class="likebtn-wrapper" data-theme="large" data-lang="ru" data-identifier="{{$photo->user->id}}"></span>
        <div class="row">
            <div id="comments-block" class="col-8 scrollit">
                @foreach($photo->comments as $comment)
                    <x-comment :comment="$comment"></x-comment>
                @endforeach
            <div class="col-4">
                <div class="comment-form">
                    <form id="create-comment-{{$photo->id}}">
                        @csrf
                        <input type="hidden" id="photo_id-{{$photo->id}}" value="{{$photo->id}}">
                        <div class="form-group">
                            <label for="body">Комментарий</label>
                            <textarea name="body" class="form-control" id="body" rows="3" required></textarea>
                        </div>
                        <p><select id="estimation" name="estimation">
                                <option selected disabled>Выберите оценку</option>
                                <option value="1">1</option>
                                <option value="2">2</option>
                                <option value="3">3</option>
                                <option value="4">4</option>
                                <option value="5">5</option>
                            </select></p>
                        <button  id="create-comment-btn" data-photo-id="{{$photo->id}}" type="submit" class="btn btn-outline-primary btn-sm btn-block create-comment-btn">Добавьте новый
                            комментарий
                        </button>
                    </form>
                </div>
            </div>
        </div>
        </div>
    </div>
@endsection
