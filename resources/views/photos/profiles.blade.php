@extends('layouts.app')
@section('content')
    <h1>{{$user->name}}</h1>
    @if(Auth::user()->id == $user->id)
    <h3>My photos<a href="{{route('photos.create')}}">Create new photo</a></h3>
    @endif
    <div class="container">
        <table class="table">
            <thead>
            <tr>
                <th scope="col">Photo</th>
                <th scope="col">Title</th>
                <th scope="col">Action</th>
            </tr>
            </thead>
            <tbody>
@foreach($user->photos as $photo)
        <tr>
            <td>
                <img src="{{asset('storage/'.$photo->image)}}" class="card-img" style="border-radius: 300px; height: 300px; width: 300px; margin-left: -6px; margin-top: -2px"  alt="{{$photo->id}}">
            </td>
            <td>
                {{$photo->title}}
            </td>
            <td>
                @if(Auth::user()->id == $user->id)
               <a href="{{route('photos.edit', ['photo' => $photo])}}">Edit</a>
                @endif
                <br>
                <a href="{{route('photos.show', ['photo' => $photo])}}"> Remove</a>
                <br>
                    @if(Auth::user()->id == $user->id)
                <form method="post" action="{{route('photos.destroy',['photo' => $photo])}}">
                    @method('delete')
                    @csrf
                    <button type="submit">delete</button>
                </form>
                        @endif
            </td>
        </tr>
            </tbody>
            @endforeach
        </table>
    </div>

@endsection
