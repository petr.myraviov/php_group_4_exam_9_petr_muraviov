<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Photo;
use Faker\Generator as Faker;
use Illuminate\Support\Facades\Storage;
use Intervention\Image\Facades\Image;

/**
 * @param int $image_number
 * @return string
 */
function photo_path(int $image_number):string
{
    $path = storage_path() ."/seed_images/" . "$image_number" . ".jpg";
    $image_name = md5($path). ".jpg";
    $resize = Image::make($path)->fit(300)->encode('jpg');
    Storage::disk('public')->put('image/'.$image_name, $resize->__toString());
    return 'image/'. $image_name;
}
$factory->define(Photo::class, function (Faker $faker) {
    return [
        'image' => photo_path(rand(1,8)),
        'title' => $faker->title,
        'user_id' => rand(1,5),
    ];
});
